import { render } from 'react-dom';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Router, Route, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import store from './store';

import FeedPage from './components/pages/FeedPage';
import LoginPage from './components/pages/LoginPage';
import UsersPage from './components/pages/UsersPage';
import UserPage from './components/pages/UserPage';
import LogoutPage from './components/pages/LogoutPage';
import FollowingsPage from './components/pages/FollowingsPage';
import FollowersPage from './components/pages/FollowersPage';


const history = syncHistoryWithStore(hashHistory, store)

class AppRouter extends Component {
    requireAuth() {
        if (!store.getState().app.myUser.id) {
            hashHistory.push('login')
        }
    }

    render() {
        return <Provider store={store}>
            <Router history={history}>
                <Route path='/' component={FeedPage} onEnter={this.requireAuth} />
                <Route path='login' component={LoginPage} />
                <Route path='users' component={UsersPage} onEnter={this.requireAuth} />
                <Route path='user/:name' component={UserPage} onEnter={this.requireAuth} />
                <Route path='user/:name/followers' component={FollowersPage} onEnter={this.requireAuth} />
                <Route path='user/:name/followings' component={FollowingsPage} onEnter={this.requireAuth} />
                <Route path='logout' component={LogoutPage} onEnter={this.requireAuth} />
            </Router>
        </Provider>
    }
}

render(<AppRouter />, document.getElementById('app'));
