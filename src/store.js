import{ createStore, combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import reducer from './reducers';

let state = {
    app: {
        tweets: {},
        users: {},
        myUser: {},
        followings: []
    },
    routing: {}
}

let appData;
const cachedData = localStorage.getItem('data');
if (cachedData) {
    appData = JSON.parse(cachedData);
    state.app = {...state.app, ...appData};
}

const rootReducer = combineReducers({
    routing: routerReducer,
    app: reducer
})

const store = createStore(rootReducer, state,
    window.devToolsExtension ? window.devToolsExtension() : undefined
)

store.subscribe( () => {
    const state = store.getState();
    const data = JSON.stringify(state.app);
    localStorage.setItem('data', data);
})


export default store;

