import _ from 'lodash';



const followerReducer = (state = {}, action) => {

    if (action.type == 'FOLLOW') {
        const index = _(state.followings).findIndex( (f) => f.followingName == action.followingName && f.followerName == action.followerName );
        if (index >= 0) {
            return state;
        }

        const newFollowing = {
            followerName: action.followerName,
            followingName: action.followingName
        };

        const newFollowings = [...state.followings, newFollowing];
        return {...state, followings: newFollowings};
    }

    if (action.type == 'UNFOLLOW') {
        const index = _(state.followings).findIndex( (f) => f.followingName == action.followingName && f.followerName == action.followerName );
        const newFollowings = [..._(state.followings).slice(0, index), ..._(state.followings).slice(index + 1)];
        const newState = {...state, followings: newFollowings};

        return newState;
    }

    return state;
}

export default followerReducer;
