import uuid from 'uuid';


const tweetReducer = (state = {}, action) => {
    if (action.type == 'NEW_TWEET') {
        const id = uuid.v1();
        const tweets = {
            [id]: {
                id,
                text: action.text,
                authorId: state.myUser.id
            },
            ...state.tweets
        }

        return {...state, tweets}
    }

    if (action.type == 'RETWEET') {
        const tweet = state.tweets[action.tweetId];
        const id = uuid.v1();

        const tweets = {
            [id]: {
                ...tweet,
                id,
                retweetedFromId: state.myUser.id
            },
            ...state.tweets
        }

        return {...state, tweets}
    }

    return state
}

export default tweetReducer;
