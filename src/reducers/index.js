import reduce from 'reduce-reducers';

import tweets from './tweet-reducer';
import auth from './auth-reducer';
import follow from './following-reducer';


export default reduce(tweets, auth, follow)
