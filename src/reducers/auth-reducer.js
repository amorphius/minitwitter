import _ from 'lodash';
import uuid from 'uuid';
import color from 'randomcolor';

const authReducer = (state = {}, action) => {
    if (action.type == 'LOGIN') {
        if (!_(action.username).trim()) {
            return state;
        }

        const existed = _(state.users).find( u => u.name == action.username);

        if (existed) {
            const newState = {...state, myUser: {...existed}};
            return newState;
        } else {
            const id = uuid.v1();
            const newUser = {
                id,
                name: action.username,
                avatar: color()
            }

            const newState = {...state, users: {...state.users, [id]: {...newUser}}, myUser: {...newUser}};
            return newState;
        }
    }

    if (action.type == 'LOGOUT') {
        const newState = {...state, myUser: {}};
        return newState;
    }

    return state
}

export default authReducer;
