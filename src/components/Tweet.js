import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';


const TweetView = ({ id, authorId, avatar, name, text, myUser, retweetFrom, onRetweetClick }) => {
    const avatarStyle = {display: 'inline-block', width: '40px', height: '40px', backgroundColor: avatar};

    return <li style={{ marginBottom: '30px' }} >
        <Link to={`user/${name}`}>
            <div style={avatarStyle}></div>&nbsp;
            <span>{name}</span>
            {retweetFrom ?
                <span>(RT from {retweetFrom})</span>
            : null}
        </Link>
        <div>{text}</div>
        {authorId != myUser.id ?
            <button onClick={ () => onRetweetClick(id) }>Retweet</button>
        : null }
    </li>
}

const Tweet = connect(
    (state, props) => {
        const tweet = props.tweet;
        const user = state.app.users[tweet.authorId];
        let retweetFrom;
        if (tweet.retweetedFromId) {
            retweetFrom = state.app.users[tweet.retweetedFromId].name;
        }

        return {
            id: tweet.id,
            text: tweet.text,
            retweetFrom,
            authorId: user.id,
            avatar: user.avatar,
            name: user.name,
            myUser: state.app.myUser
        }
    },
    dispatch => ({
        onRetweetClick: (id) => {
            dispatch({
                type: 'RETWEET',
                tweetId: id
            })
        }
    })
)(TweetView);


export default Tweet;
