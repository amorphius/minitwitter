import _ from 'lodash';
import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { hashHistory } from 'react-router';



class UserInfo extends Component {
    onFollowersClick() {
        hashHistory.push(`user/${this.props.name}/followers`);
    }

    onFollowingsClick() {
        hashHistory.push(`user/${this.props.name}/followings`);
    }

    render() {
        const { userId, avatar, name, followerNames, followingNames, myUser, onFollow, onUnfollow } = this.props;
        const avatarStyle = {display: 'inline-block', width: '40px', height: '40px', backgroundColor: avatar};
        const isFollowed = _(followerNames).includes(myUser.name);

        return <section>
            <div>
                <div style={avatarStyle}></div>
                &nbsp;
                <span>{name}</span>
            </div>
            <div>Followers:
                {followerNames.length > 0 ?
                    <a href='javascript:void(0);' onClick={ () => this.onFollowersClick() }>{followerNames.length}</a>
                : <span>{followerNames.length}</span>
                }
            </div>
            <div>Followings:
                {followingNames.length > 0 ?
                    <a href='javascript:void(0);' onClick={ () => this.onFollowingsClick() }>{followingNames.length}</a>
                : <span>{followingNames.length}</span>
                }
            </div>
            {userId !== myUser.id ?
                (isFollowed ?
                    <a href='javascript:void(0);' onClick={ () => onUnfollow(myUser.name, name) }>Unfollow</a>
                :
                    <a href='javascript:void(0);' onClick={ () => onFollow(myUser.name, name) }>Follow</a>
                )
            : null }
        </section>
    }
}

const UserInfoProvider = connect(
    (state, props) => {
        const followerNames = _(state.app.followings)
            .filter( (f) => f.followingName == props.user.name )
            .map( (f) => f.followerName )
            .value();

        const followingNames = _(state.app.followings)
            .filter( (f) => f.followerName == props.user.name )
            .map( (f) => f.followingName )
            .value();

        return {
            userId: props.user.id,
            myUser: state.app.myUser,
            avatar: props.user.avatar,
            name: props.user.name,
            followerNames,
            followingNames
        }
    },
    dispatch => ({
        onFollow: (followerName, followingName) => dispatch({
            type: 'FOLLOW',
            followerName,
            followingName
        }),
        onUnfollow: (followerName, followingName) => dispatch({
            type: 'UNFOLLOW',
            followerName,
            followingName
        })
    })
)(UserInfo);

export default UserInfoProvider;
