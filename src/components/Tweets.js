import _ from 'lodash';
import React from 'react';
import Tweet from './Tweet';



export default ({ tweets, title }) => {
    const els = _(tweets).toArray().map((tweet) => (
        <Tweet key={tweet.id} tweet={tweet} />
    )).value()

    return <section>
        {title ?
            <h3>{title}</h3>
        : null}
        {!_(tweets).isEmpty() ?
            <ul>
                {els}
            </ul>
        : null}

        {_(tweets).isEmpty() ?
            <h4>No tweets</h4>
        : null}
    </section>
}
