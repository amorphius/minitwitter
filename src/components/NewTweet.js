import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';


class NewTweetView extends Component {
    onSubmit() {
        if (_(this.refs.tweet.value).trim()) {
            this.props.onSubmit(this.refs.tweet.value);
            this.refs.tweet.value = '';
        }
    }

    render() {
        return <section>
            <textarea ref="tweet"></textarea><br />
            <button onClick={ () => this.onSubmit() }>Tweet</button>
        </section>
    }
}

const NewTweet = connect(null, dispatch => ({
    onSubmit: (text) => {
        dispatch({
            type: 'NEW_TWEET',
            text
        })
    }
}))(NewTweetView);


export default NewTweet;
