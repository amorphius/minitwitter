import React, { Component } from 'react';
import { connect } from 'react-redux';

import Logout from '../Logout';
import Tweets from '../Tweets';
import UserInfo from '../UserInfo';


const UserPage = ({ params, tweets, user }) => (
    <section>
        <Logout />
        <UserInfo user={user} />
        <Tweets tweets={tweets} />
    </section>
)

const UserPageProvider = connect(
    (state, props) => {
        const user = _(state.app.users).find( (u) => u.name == props.params.name );
        const tweets = _(state.app.tweets)
            .filter( (t) =>
                (t.authorId == user.id && !t.retweetedFromId) || t.retweetedFromId == user.id
            ).value();
        return {tweets, user}
    }
)(UserPage);

export default UserPageProvider;
