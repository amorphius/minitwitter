import _ from 'lodash';
import { connect } from 'react-redux';

import UsersPage from './UsersPage';

const FollowersPage = connect(
    (state, props) => {
        const userName = props.params.name;
        const user = _(state.app.users).find( (u) => u.name == userName );
        const followers = _(state.app.followings)
            .filter( (f) => f.followingName == user.name )
            .map( (f) => f.followerName )
            .map( (name) => _(state.app.users).find( (u) => u.name == name ) )
            .value();

        return {
            title: `Followers of ${userName}`,
            users: followers
        }
    }
)(UsersPage);

export default FollowersPage;
