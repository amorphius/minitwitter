import React, { Component } from 'react';
import { connect } from 'react-redux';
import { hashHistory } from 'react-router';

class LogoutPage extends Component {
    onLogout() {
        this.props.onLogout();
        hashHistory.push('/');
    }

    render() {
        return <section>
            <button onClick={() => this.onLogout() }>Logout</button>
        </section>
    }
}

const LogoutPageProvider = connect(
    null,
    dispatch => ({
        onLogout: () => {
            dispatch({
                type: 'LOGOUT'
            })
        }
    })
)(LogoutPage)

export default LogoutPageProvider;
