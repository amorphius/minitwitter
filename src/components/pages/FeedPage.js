import React, { Component } from 'react';
import Logout from '../Logout';
import { connect } from 'react-redux';
import _ from 'lodash';

import Tweets from '../Tweets';
import NewTweet from '../NewTweet';
import UserInfo from '../UserInfo';


const FeedPage = ({tweets, myUser, title}) => (
    <section>
        <Logout />
        <h1>Feed page</h1>
        <UserInfo user={myUser} />
        <NewTweet />
        <Tweets tweets={tweets} title={title} />
    </section>
)

const FeedPageProvider = connect(
    state => {
        const followingIds = _(state.app.followings)
            .filter( (f) => f.followerName == state.app.myUser.name )
            .map( (f) => {
                const name = f.followingName;
                return _(state.app.users).find( u => u.name == name ).id;
            })
            .value();

        let tweets, title;
        if (!followingIds.length) {
            tweets = state.app.tweets;
            title = 'You dont follow anyone, you see all tweets from all users';
        } else{
            tweets = _(state.app.tweets).toArray()
                .filter( (t) =>
                    _(followingIds).includes(t.authorId) ||
                        t.authorId == state.app.myUser.id ||
                        _(followingIds).includes(t.retweetedFromId) ||
                        t.retweetedFromId == state.app.myUser.id
                ).value();
        }

        return {tweets, myUser: state.app.myUser, title}
    }
)(FeedPage);

export default FeedPageProvider;
