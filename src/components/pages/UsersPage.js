import React from 'react';

import UserInfo from '../UserInfo';
import Logout from '../Logout';


const UsersPage = ({ title, users }) => (
    <section>
        <Logout />
        {title ?
            <h3>{title}</h3>
        : null}

        {users.map( (u) => (
            <UserInfo key={u.id} user={u} />
        ))}
    </section>
)

export default UsersPage;
