import _ from 'lodash';
import { connect } from 'react-redux';

import UsersPage from './UsersPage';

const FollowingsPage = connect(
    (state, props) => {
        const userName = props.params.name;
        const user = _(state.app.users).find( (u) => u.name == userName );
        const followings = _(state.app.followings)
            .filter( (f) => f.followerName == user.name )
            .map( (f) => f.followingName )
            .map( (name) => _(state.app.users).find( (u) => u.name == name ) )
            .value();

        return {
            title: `Followings of ${userName}`,
            users: followings
        }
    }
)(UsersPage);

export default FollowingsPage;
