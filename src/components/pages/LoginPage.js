import React, { Component } from 'react';
import { connect } from 'react-redux';
import { hashHistory } from 'react-router'


class LoginPage extends Component {
    onLogin() {
        const username = this.refs.username.value;
        this.props.onLogin(username);
    }

    render() {
        if (this.props.myUser.id) {
            hashHistory.push('/');
            return null
        }

        return <section>
            <input ref="username" />
            <button onClick={ () => this.onLogin() }>Login</button>
        </section>
    }
}


LoginPage = connect(
    (state, props) => {
        const myUser = state.app.myUser;
        return {myUser}
    },
    dispatch => ({
        onLogin: (username) => {
            dispatch({
                type: 'LOGIN',
                username
            })
        }
    })
)(LoginPage)

export default LoginPage;
