import React, { Component } from 'react';
import { connect } from 'react-redux';
import { hashHistory } from 'react-router';


const Logout = ({ name, onLogout }) => (
    <section>
        <span>
            You logged in as {name}
        </span>
        &nbsp;
        <button onClick={ () => hashHistory.push('/') }>Home</button>
        <button onClick={ () => onLogout() }>Logout</button>
    </section>
)

const LogoutProvider = connect(
    state => ({
        name: state.app.myUser.name
    }),
    dispatch => ({
        onLogout: () => {
            dispatch({
                type: 'LOGOUT'
            });

            hashHistory.push('logout');
        }
    })
)(Logout);

export default LogoutProvider;

