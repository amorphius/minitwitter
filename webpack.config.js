const argv = require('minimist-argv');


config = {
    entry: './src/entry',
    output: {
        filename: 'bundle.js',
        path: 'dist'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel'
            }
        ]
    },
    resolve: {
        extensions: ['', '.js']
    }
}

if (!argv.nowatch)
    config.watch = true;

module.exports = config;
